#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionAbout, SIGNAL(triggered()), this, SLOT(aboutPress()));
}
MainWindow::~MainWindow()
{
	delete ui;
}
void MainWindow::aboutPress()
{
	QMessageBox::aboutQt(this, "Hello");
}
